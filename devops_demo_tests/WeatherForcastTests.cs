using System;
using Xunit;
using static devops_demo.Controllers.SampleDataController;

namespace devops_demo_tests
{
    public class WeatherForcastTests
    {
        [Fact]
        public void CelciusToFareinheightSucess()
        {
            var Target = new WeatherForecast();
            Target.TemperatureC = 30;

            var Expected = 85;
            var Actual = Target.TemperatureF;
            
            Assert.Equal(Expected, Actual);
        }
    }
}
